<?php /* Smarty version Smarty-3.1.19, created on 2015-02-18 21:37:11
         compiled from "templates\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:195254e4f7a17de681-71916362%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '64b45625488c43747e928a63aa9fa703804501de' => 
    array (
      0 => 'templates\\index.tpl',
      1 => 1424291829,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '195254e4f7a17de681-71916362',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_54e4f7a1f02976_65960262',
  'variables' => 
  array (
    'bodyclass' => 0,
    'errors' => 0,
    'error' => 0,
    'content' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54e4f7a1f02976_65960262')) {function content_54e4f7a1f02976_65960262($_smarty_tpl) {?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Project</title>
		<base href="http://localhost/project/" >
		
		<!-- Bootstrap -->

		<script type="text/javascript" src="templates/js/jquery-1.11.0.js"></script>
		<script type="text/javascript" src="templates/js/bootstrap.min.js"></script>
		<link href="templates/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="templates/css/styles.css" rel="stylesheet" media="screen">
		<link href="templates/css/sb-admin.css" rel="stylesheet" media="screen">
		<link href="templates/fonts/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	</head>
	<body class="<?php echo $_smarty_tpl->tpl_vars['bodyclass']->value;?>
">
	
		<div class="container error_holder">
		<?php if (isset($_smarty_tpl->tpl_vars['errors']->value)&&count($_smarty_tpl->tpl_vars['errors']->value)) {?>
			<ul class="errors">
			<?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['errors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value) {
$_smarty_tpl->tpl_vars['error']->_loop = true;
?>
				<li><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</li>
			<?php } ?>
			</ul>
		<?php }?>
		</div>
		
		<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

	</body>
</html><?php }} ?>

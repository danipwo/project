<?php /* Smarty version Smarty-3.1.19, created on 2015-02-18 21:31:55
         compiled from "..\templates\admin\login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2360454e3a724dd77d2-14500881%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0528caecde618614193904272aa2f3d42990d67e' => 
    array (
      0 => '..\\templates\\admin\\login.tpl',
      1 => 1424291513,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2360454e3a724dd77d2-14500881',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_54e3a725219147_02368118',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54e3a725219147_02368118')) {function content_54e3a725219147_02368118($_smarty_tpl) {?><div class="container">

	<form class="form-signin" action="" method="post" target="_self">
		<h2 class="form-signin-heading">Please sign in</h2>
		<input name="username" type="text" autofocus="" required="" placeholder="User name" class="form-control">
		<input name="password" type="password" required="" placeholder="Password" class="form-control">
		
		<div class="text-center">
			<input type="radio" name="type" value="teacher" id="teacher" checked />
			<label for="teacher">Teacher</label>
			<input type="radio" name="type" value="student" id="student" />
			<label for="student">Student</label>
		</div>
		
		<input type="hidden" name="action" value="login" />
		<button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
		<a href="register.php" class="btn btn-lg btn-primary btn-block">Register</a>
    </form>

</div><?php }} ?>

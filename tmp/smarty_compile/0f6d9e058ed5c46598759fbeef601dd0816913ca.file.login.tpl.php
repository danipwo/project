<?php /* Smarty version Smarty-3.1.19, created on 2015-02-19 15:14:06
         compiled from "templates/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:36342621654e5d38e9c9216-86551034%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0f6d9e058ed5c46598759fbeef601dd0816913ca' => 
    array (
      0 => 'templates/login.tpl',
      1 => 1424345273,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '36342621654e5d38e9c9216-86551034',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_54e5d38ebebba7_98901395',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54e5d38ebebba7_98901395')) {function content_54e5d38ebebba7_98901395($_smarty_tpl) {?><div class="container">

	<form class="form-signin" action="" method="post" target="_self">
		<h2 class="form-signin-heading">Please sign in</h2>
		<input name="username" type="text" autofocus="" required="" placeholder="User name" class="form-control">
		<input name="password" type="password" required="" placeholder="Password" class="form-control">
		
		<div class="text-center">
			<input type="radio" name="type" value="teacher" id="teacher" checked />
			<label for="teacher">Teacher</label>
			<input type="radio" name="type" value="student" id="student" />
			<label for="student">Student</label>
		</div>
		
		<input type="hidden" name="action" value="login" />
		<button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
		<a href="register.php" class="btn btn-lg btn-primary btn-block">Register</a>
    </form>

</div><?php }} ?>

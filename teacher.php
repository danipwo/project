<?php
	require_once('config.php');
	$Administration = new Administration();

	if($Administration->isTeacher()){
		$Administration->showDashboard();
	}
	else {
		$Administration->redirectToLogin();
	}

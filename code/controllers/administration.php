<?php

class Administration {

	public $template;
	public $errors;
	
	public function __construct() {
		$this->template = new Smarty();
		$this->template->setTemplateDir('templates/');
		$this->template->setCompileDir('tmp/smarty_compile/');
		$this->template->setConfigDir('config/');
		$this->template->setCacheDir('cache/');
		
		$this->errors = array();
		
		$this->handleRequests();
	}
	
	public function isLoggedIn() {
		
		if($user = $this->getUser()) {
	
			return $user;
		}
	
		return false;
		
	}
	
	public function isTeacher() {
		
		if($user = $this->getUser() && $user->isTeacher()) {
	
			return $user;
		}
	
		return false;
	}
	
	public function isStudent() {
		
		if($user = $this->getUser() && $user->isStudent()) {
	
			return $user;
		}
	
		return false;
	}
	
	public function handleRequests() {
		if(isset($_REQUEST['action'])) {
			switch($_REQUEST['action']) {
				case 'login':
					$this->doLogin();
				break;
				case 'register':
					$this->doRegister();
				break;
				case 'logout':
					$this->doLogout();
				break;
			}
		}
	}
	
	public function addError($error) {
		$this->errors []= $error;
		return false;
	}
	
	public function doLogin() {
		if(isset($_POST['username']) && 
		   isset($_POST['password']) && 
		   isset($_POST['type']) && 
		   $user = User::doLogin(trim($_POST['username']), trim($_POST['password']), trim($_POST['type']))) {
			return $user;
		}
		
		return $this->addError('Please input a valid username and password');
	}
	
	public function doRegister() {
		if(isset($_POST['type']) && trim($_POST['type']) &&
		   isset($_POST['firstname']) && trim($_POST['firstname']) &&
		   isset($_POST['lastname']) && trim($_POST['lastname']) &&
		   isset($_POST['email']) && trim($_POST['email']) &&
		   isset($_POST['username']) && trim($_POST['username']) &&
		   isset($_POST['password']) && trim($_POST['password']) ) {
				
				if($_POST['type'] == "teacher") {
					$type = "teacher";
				}	
				else {
					$type = "student";
				}
				
				$result = $GLOBALS['DB']->selectQuery('*',
													  $type, 
													  'username LIKE "'.trim($GLOBALS['DB']->escape($_POST['username'])).'"');
													  
				if($result) {
					return $this->addError('Username already used');	
				}
				else {		  
					if($type == "teacher") {
						$user = new Teacher();
					}
					else {
						$user = new Student();
					}
					$user->setFields($_POST);
					$user->save();
					return $this->doLogin();
					
				}
											
		}
		
		return $this->addError('Please input all fields');
	}
	
	public function doLogout() {
		unset($_SESSION['user']);
		$this->redirectToLogin();
	}
	
	public function showLogin() {
		
		$htmlContent = $this->template->fetch('login.tpl');
		echo $this->showPage($htmlContent, 'login');
	}	
	
	public function showRegister() {
		
		$htmlContent = $this->template->fetch('register.tpl');
		echo $this->showPage($htmlContent, 'register');
		
	}
	
	public function showDashboard() {
		$user = $this->getUser();
		
		if($user->isTeacher()) {
			$this->template->assign('teacher', $user);
			$htmlContent = $this->template->fetch('teacher.tpl');
		}
		else {
			$this->template->assign('teacher', $user);
			$htmlContent = $this->template->fetch('student.tpl');
			
		}
		echo $this->showPage($htmlContent, 'dashboard');
		
	}
	
	public function showPage($content = '', $bodyClass = '') {
				
		$this->template->assign('content', $content);
		$this->template->assign('bodyclass', $bodyClass);
		$this->template->assign('errors', $this->errors);
		$this->template->display('index.tpl');
	}
	
	public function getUser() {
		if(isset($_SESSION['user'])) {
			$user = $_SESSION['user'];
			if($user->isTeacher()) {
				return new Teacher($user->id);
			}
			else {
				return new Student($user->id);
			}
		}
		return false;
	}
	
	public function redirectToLogin() {
		header("Location: index.php");
		die();
	}
}

?>
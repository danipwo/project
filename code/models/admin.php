<?php

class Admin extends Model {

	protected static $table = 'admin';
	
	public function __construct($id) {
		parent::__construct($id);
	}
	
	public function __get($property) {
		return parent::__get($property);
	}
	
	public function __set($property, $value) {
		return parent::__set($property, $value);
	}
	
	public static function doLogin($username, $password) {
		$result = $GLOBALS['DB']->selectQuery('*', 
											  'admin', 
											  'username LIKE "'.trim($GLOBALS['DB']->escape($_POST['username'])).'" AND 
											  password LIKE "'.md5($GLOBALS['DB']->escape($_POST['password'])).'" AND 
											  active = 1');
												  
		if(count($result)) {
			$adminObj = new Admin($result[0]['id']);
			$_SESSION['admin'] = $adminObj;
			return true;
		}
		
		return false;
	}
}

?>
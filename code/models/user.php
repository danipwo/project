<?php

class User extends Model {
	
	public $username;
	public $active;
	public $firstname;
	public $lastname;
	public $email;
	public $password;
	
	public function __construct($id = null) {
		parent::__construct($id);
	}
	
	public function isStudent() {
		if(strtolower(get_class($this)) == 'student')
			return true;
		return false;
	}
	
	public function isTeacher() {
		if(strtolower(get_class($this)) == 'teacher')
			return true;
		return false;
	}
	
	public function getName() {
		return $this->firstname . ' ' . $this->lastname;
	}
	
	public static function doLogin($username, $password, $type) {
		//$GLOBALS['DB']->debug = true;
		$result = $GLOBALS['DB']->selectQuery('*', 
											  $type, 
											  'username LIKE "'.trim($GLOBALS['DB']->escape($_POST['username'])).'" AND 
											  password LIKE "'.$GLOBALS['DB']->escape($_POST['password']).'"');
		//die(dump($result));										  
		if(count($result)) {
			
			if($type == "teacher") {
				$user = new Teacher($result[0]['id']);
			}
			else {
				$user = new Student($result[0]['id']);
			}
			if($user) {
				$_SESSION['user'] = $user;
				return $user;
			}
		}
		
		return false;
	}
	
	public static function doRegister($data) {
														  
		if(count($result)) {
			if($type == "teacher") {
				$user = new Teacher($result[0]['id']);
			}
			else {
				$user = new Student($result[0]['id']);
			}
			if($user) {
				$_SESSION['user'] = $user;
				return $user;
			}
		}
		
		return false;
	}
}
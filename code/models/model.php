<?php

abstract class Model {

	protected static $table;
	public $id;
	
	public function __construct($id = null) {
		$table = strtolower(get_class($this));
		if($id) {
			$tmpObject = $GLOBALS['DB']->selectQuery('*', $table, ' id = '.$id);
			if(count($tmpObject)){
				$objParameters = array_keys($tmpObject[0]);
				foreach($objParameters as $parameter) {
					$this->{$parameter} = $tmpObject[0][$parameter];
				}				
			}
		}
	}
	
	public function setFields($fields) {
		foreach($fields as $property => $value) {
			if(property_exists($this, $property)) {
				$this->{$property} = $value;
			}
		}
	}
	
	public function save() {
		$fields = array();
		$table = strtolower(get_class($this));
		$columns = $GLOBALS['DB']->getColumns($table);
		
		foreach($columns as $column) {
			if(property_exists($this, $column['Field']) && 
			   !is_null($this->{$column['Field']}) && 
			   !in_array($column['Field'], array('id', 'crdate'))) {
				$fields[$column['Field']] = $this->{$column['Field']};
			}
		}
		
		if(count($fields)) {
			if(!is_null($this->id)) {
				$GLOBALS['DB']->updateQuery($table, $fields, $this->id);
			}
			else {
				$class = get_class($this);
				$user = new $class($GLOBALS['DB']->insertQuery($table, $fields));
			}
		}
	}
	
	/*
	public function __get($property) {
		$property = strtolower($property);
		if (isset($this->attributes[$property])) {
			return $this->attributes[$property];
		}
		return null;
    }

    public function __set($property, $value) {
        $property = strtolower($property);
		if (isset($this->attributes[$property])) {
            $this->attributes[$property] = $value;
			return true;
		}
		return false;
    }
	*/
}

?>
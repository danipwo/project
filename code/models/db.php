<?php

class DB extends mysqli {
	
    private static $instance = null;
	public $debug = false;
	
	public static function getInstance() {
		if (!self::$instance instanceof self) {
			self::$instance = new self;
		}
		return self::$instance;
	}
	
	public function escape($string) {
		return $this->real_escape_string($string);
	}
	
	/*
	Establish the database connection 
	*/
	
	private function __construct() {
		parent::__construct(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if (mysqli_connect_error()) {
			exit('Connect Error (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		parent::set_charset('utf-8');
	}
	
	/*
	Creates a SELECT database query and returns the resulted rows
	*/
	public function selectQuery($fields, $from_table, $where = '', $group_by = '', $order_by = '', $limit = '') {
		
		$arrResults = array();
		
		if($where) {
			//$where = ' AND '.$this->real_escape_string($where);
			$where = ' AND '.$where;
		}
		
		$queryString = 'SELECT '.$fields.' FROM '.$from_table.' WHERE 1 = 1 '.$where.' ';
		
		if($group_by) {
			$queryString .= ' GROUP BY '.$group_by.' ';
		}
		
		if($group_by) {
			$queryString .= ' ORDER BY '.$order_by.' ';
		}
		
		if($limit) {
			$queryString .= ' LIMIT '.$limit.' ';
		}
		
		if($this->debug) {
			var_dump('Query: '.$queryString);
		}
		
		$query = $this->query($queryString);
		
		if($query) {
			
			while($row = $query->fetch_assoc()) {
				$arrResults []= $row;
			}
		}
		elseif(DEV_MODE) {
			var_dump('Error: '.$this->error);
			var_dump('Query: '.$queryString);
		}
		
		
		return $arrResults;
	}
	
	public function insertQuery($table, $fields) {
		$field_names = implode(',', array_keys($fields));
		$field_values = array_values($fields);
		foreach($field_values as &$value) {
			$value = '"'.$value.'"';
		}
		$field_values = implode(',', $field_values);
		$this->query('INSERT INTO '.$table.' ('.$field_names.') VALUES ('.$field_values.') ');
		return $this->insert_id;
	}
	
	public function updateQuery($table, $fields, $id) {
		
		$update = array();
		foreach($fields as $field_key => $field) {
			$update[] = $field_key . ' = "' . $field . '"';
		}
		$update = implode(',', $update);
		$this->query('UPDATE '.$table.' SET '.$update.' WHERE id = '.$id.' ');
	}
	
	public function getColumns($table) {
		$query = $this->query('SHOW COLUMNS FROM '.$table);
		
		if($query) {
			
			while($row = $query->fetch_assoc()) {
				$arrResults []= $row;
			}
		}
		elseif(DEV_MODE) {
			var_dump('Error: '.$this->error);
			var_dump('Query: '.$queryString);
		}
		
		
		return $arrResults;
		
	}
}

?>
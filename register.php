<?php
	require_once('config.php');
	$Administration = new Administration();

	if($Administration->isLoggedIn()){
		$Administration->showDashboard();
	}
	else {
		$Administration->showRegister();
	}
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Project</title>
		
		<!-- Bootstrap -->

		<script type="text/javascript" src="templates/js/jquery-1.11.0.js"></script>
		<script type="text/javascript" src="templates/js/bootstrap.min.js"></script>
		<link href="templates/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="templates/css/styles.css" rel="stylesheet" media="screen">
		<link href="templates/css/sb-admin.css" rel="stylesheet" media="screen">
		<link href="templates/fonts/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	</head>
	<body class="{$bodyclass}">
	
		<div class="container error_holder">
		{if isset($errors) && count($errors)}
			<ul class="errors">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
		{/if}
		</div>
		
		{$content}
	</body>
</html>
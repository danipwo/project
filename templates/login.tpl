<div class="container">

	<form class="form-signin" action="" method="post" target="_self">
		<h2 class="form-signin-heading">Please sign in</h2>
		<input name="username" type="text" autofocus="" required="" placeholder="User name" class="form-control">
		<input name="password" type="password" required="" placeholder="Password" class="form-control">
		
		<div class="text-center">
			<input type="radio" name="type" value="teacher" id="teacher" checked />
			<label for="teacher">Teacher</label>
			<input type="radio" name="type" value="student" id="student" />
			<label for="student">Student</label>
		</div>
		
		<input type="hidden" name="action" value="login" />
		<button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
		<a href="register.php" class="btn btn-lg btn-primary btn-block">Register</a>
    </form>

</div>
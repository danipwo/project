<div class="container">

	<form class="form-signin" action="" method="post" target="_self">
		<h2 class="form-signin-heading">Please complete all the fields</h2>
		<select name="type" class="form-control">
			<option value="teacher">Teacher</option>
			<option value="student">Student</option>
		</select>
		<input name="firstname" type="text" autofocus="" required="" placeholder="First Name" class="form-control">
		<input name="lastname" type="text" autofocus="" required="" placeholder="Last Name" class="form-control">
		<input name="email" type="text" autofocus="" required="" placeholder="Email" class="form-control">
		<input name="username" type="text" autofocus="" required="" placeholder="User name" class="form-control">
		<input name="password" type="password" required="" placeholder="Password" class="form-control">
		<input type="hidden" name="action" value="register" />
		<button type="submit" class="btn btn-lg btn-primary btn-block">Register</button>
    </form>

</div>
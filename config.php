<?php

/* START CONSTANTS */

define("DEV_MODE", true);
define("ADMIN_MODE", true);
define("SLASH", '/');
define("ROOT", dirname(__FILE__).'/');

if(DEV_MODE) {
	error_reporting(-1);
}

/* Database Connection */
define("DB_HOST", "localhost");
define("DB_USER", "andi_project");
define("DB_PASS", "8tr6xna0g18o");
define("DB_NAME", "andi_project");

/* END CONSTANTS */

session_start();

/* Autoload Behavior for classes */
function __autoload($name) {
	
	if(is_file(ROOT.'code/controllers/'.strtolower($name).'.php')) {
		require_once ROOT.'code/controllers/'.strtolower($name).'.php';
	}
	
	if(is_file(ROOT.'code/models/'.strtolower($name).'.php')) {
		require_once ROOT.'code/models/'.strtolower($name).'.php';
	}
}

spl_autoload_register("__autoload");

/* All-purpose debugging function */
function dump($var) {
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
}

/* SMARTY Configuration */
require_once('libraries/smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir('templates/');
$smarty->setCompileDir('tmp/smarty_compile/');
$smarty->setConfigDir('config/');
$smarty->setCacheDir('cache/');


$GLOBALS['DB'] = DB::getInstance();

?>